#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "grpcclient.h"

namespace Ui {
class MainWindow;
}

#include <QTimer>

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

protected slots:
	void timeout();
private slots:
	void on_btn_pressed();
	void on_btn_released();
	
	void on_slider_zoom_speed_valueChanged(int value);
	void on_slieder_focus_speed_valueChanged(int value);
	void on_slider_iris_pos_valueChanged(int value);

	void on_comboBox_lens_type_currentIndexChanged(int index);
	void on_comboBox_encoder_type_activated(int index);

	void on_line_focus_pos_returnPressed();

	void on_line_zoom_pos_returnPressed();

	void on_line_cam_ip_returnPressed();

private:
	Ui::MainWindow *ui;
	GrpcClient *client;
	int focusSpeed;
	int zoomSpeed;
	
	bool isIRIS;
	QTimer *timer;
};

#endif // MAINWINDOW_H
