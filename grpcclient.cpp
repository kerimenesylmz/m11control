#include "grpcclient.h"

#include <QDebug>

#include <grpc/grpc.h>
#include <grpc++/channel.h>
#include <grpc++/client_context.h>
#include <grpc++/create_channel.h>
#include <grpc++/security/credentials.h>

GrpcClient::GrpcClient(QString host, int port)
{
	_host = host;
	_port = port;
	QString addr = QString("%1:%2").arg(_host).arg(_port);
	grpc::string my_address = addr.toStdString();
	qDebug() << "connection to " << addr.toStdString().data();
	std::shared_ptr<grpc::Channel> chn = grpc::CreateChannel(my_address, grpc::InsecureChannelCredentials());
	mystub = m11::VideoControl::NewStub(chn);
}

int GrpcClient::StartForwardZoom(m11::ZoomQ zoom, m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	grpc::Status s = mystub->StartForwardZoom(&ctx, zoom, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "StartForwardZoom error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::StartBackwardZoom(m11::ZoomQ zoom, m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	grpc::Status s = mystub->StartBackwardZoom(&ctx, zoom, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "StartBackwardZoom error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::StopZoom(m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	m11::Empty empty;
	grpc::Status s = mystub->StopZoom(&ctx, empty, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "StopZoom error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::StartForwardFocus(m11::FocusQ focus, m11::LensGeneralInfo &info)
{
	qDebug() << "START FORWARD FOCUS";
	grpc::ClientContext ctx;
	grpc::Status s = mystub->StartForwardFocus(&ctx, focus, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "StartForwardFocus error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::StartBackwardFocus(m11::FocusQ focus, m11::LensGeneralInfo &info)
{
	qDebug() << "START BACKWARD FOCUS";
	grpc::ClientContext ctx;
	grpc::Status s = mystub->StartBackwardFocus(&ctx, focus, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "StartBackwardFocus error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::StopFocus(m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	m11::Empty empty;
	grpc::Status s = mystub->StopFocus(&ctx, empty, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "StopFocus error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::OpenIRIS(m11::LensGeneralInfo &info)
{
	qDebug() << "open iris triggering";
	grpc::ClientContext ctx;
	m11::Empty empty;
	grpc::Status s = mystub->OpenIRIS(&ctx, empty, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "OpenIRIS error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::CloseIRIS(m11::LensGeneralInfo &info)
{
	qDebug() << "Close iris starting";
	grpc::ClientContext ctx;
	m11::Empty empty;
	grpc::Status s = mystub->CloseIRIS(&ctx, empty, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "CloseIRIS error msg : " << QString::fromStdString(s.error_message());
	qDebug() << "close iris finisehed";
	return s.error_code();
}

int GrpcClient::SetIRISPosition(m11::IRISQ iris, m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	qDebug() << iris.iris_position();
	grpc::Status s = mystub->SetIRISPosition(&ctx, iris, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "SetIRISPosition error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::SetH264Format(m11::StreamQ stream, m11::VideoGeneralInfo &info)
{
	grpc::ClientContext ctx;
	grpc::Status s = mystub->StartH264VideoStream(&ctx, stream, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "SetH264Format error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::SetHEVCFormat(m11::StreamQ stream, m11::VideoGeneralInfo &info)
{
	grpc::ClientContext ctx;
	grpc::Status s = mystub->StartHEVCVideoStream(&ctx, stream, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "SetHEVCFormat error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::GetStreamFormatType(m11::StreamQ stream, m11::VideoGeneralInfo &info)
{
	grpc::ClientContext ctx;
	grpc::Status s = mystub->GetVideoStreamFormat(&ctx, stream, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "SetHEVCFormat error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::OpenIrCut(m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	m11::Empty empty;
	grpc::Status s = mystub->OpenIRCutFilter(&ctx, empty, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "OpenIRCUT error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::CloseIrCut(m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	m11::Empty empty;
	grpc::Status s = mystub->CloseIRCutFilter(&ctx, empty, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "CloseIRCUT error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::StartAutoFocus(m11::AutoFocusQ afMode, m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	grpc::Status s = mystub->StartAutoFocus(&ctx, afMode, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "CloseIRCUT error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::MoveToFocusPos(m11::PosQ pos, m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	grpc::Status s = mystub->MoveToFocusPosition(&ctx, pos, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "Move focus Position error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::MoveToZoomPos(m11::PosQ pos, m11::LensGeneralInfo &info)
{
	grpc::ClientContext ctx;
	grpc::Status s = mystub->MoveToZoomPosition(&ctx, pos, &info);
	if (s.error_code() != grpc::OK)
		qDebug() << "Move Zoom Position error msg : " << QString::fromStdString(s.error_message());
	return s.error_code();
}

int GrpcClient::GetLensInfo(m11::LensInfo &info)
{
	grpc::ClientContext ctx;
	m11::Empty empty;
	grpc::Status s = mystub->GetLensInfo(&ctx, empty, &info);
	return 0;
}

int GrpcClient::getIrCutState(m11::IrCutFilter &resp)
{
	grpc::ClientContext ctx;
	m11::Empty empty;
	grpc::Status s = mystub->GetIrCutFilterState(&ctx, empty, &resp);
	return s.error_code();
}

int GrpcClient::getIrisPos(m11::IrisState &state)
{
	grpc::ClientContext ctx;
	m11::Empty empty;
	grpc::Status s = mystub->GetIrisPos(&ctx, empty, &state);
	return s.error_code();
}
