#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	isIRIS(false)
{
	timer = new QTimer();
	timer->start(1000);
	timer->setInterval(1000);
	connect(timer, SIGNAL(timeout()), SLOT(timeout()));
	ui->setupUi(this);
	
	client = NULL;
	
	connect(ui->btn_focus_in, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	connect(ui->btn_focus_out, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	connect(ui->btn_zoom_in, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	connect(ui->btn_zoom_out, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	connect(ui->btn_iris_on_off, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	connect(ui->btn_open_ir_cut, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	connect(ui->btn_close_ir_cut, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	connect(ui->btn_af_slow, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	connect(ui->btn_af_normal, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	connect(ui->btn_af_speed, SIGNAL(pressed()), this, SLOT(on_btn_pressed()));
	
	connect(ui->btn_focus_in, SIGNAL(released()), this, SLOT(on_btn_released()));
	connect(ui->btn_focus_out, SIGNAL(released()), this, SLOT(on_btn_released()));
	connect(ui->btn_zoom_in, SIGNAL(released()), this, SLOT(on_btn_released()));
	connect(ui->btn_zoom_out, SIGNAL(released()), this, SLOT(on_btn_released()));
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_btn_pressed()
{
	m11::LensGeneralInfo info;
	m11::AutoFocusQ afMode;
	m11::ZoomQ zoom;
	m11::FocusQ focus;
	zoom.set_zoom_speed(zoomSpeed);
	focus.set_focus_speed(focusSpeed);
	int ok;
	QToolButton *tool = (QToolButton *)sender();
	if (tool == ui->btn_focus_in){
		qDebug() << "press focus in";
		ok = client->StartForwardFocus(focus, info); // focus out
	} else if (tool == ui->btn_focus_out){
		qDebug() << "press focus out";
		ok = client->StartBackwardFocus(focus, info); // focus in
	} else if (tool == ui->btn_zoom_in){
		qDebug() << "press zoom in";
		ok = client->StartBackwardZoom(zoom, info); // zoom in
	}else if (tool == ui->btn_zoom_out){
		qDebug() << "press zoom out";
		ok = client->StartForwardZoom(zoom, info); // zoom out
	}else if (tool == ui->btn_iris_on_off){
		qDebug() << "press iris toggle";
		if (isIRIS)
			ok = client->CloseIRIS(info);
		else{
			ok = client->OpenIRIS(info);
		}
		isIRIS = !isIRIS;
	} else if (tool == ui->btn_open_ir_cut) {
		client->OpenIrCut(info);
	} else if (tool == ui->btn_close_ir_cut) {
		client->CloseIrCut(info);
	} else if (tool == ui->btn_af_slow) {
		afMode.set_af_mode(m11::AutoFocusQ::Mode::AutoFocusQ_Mode_AF_SLOW);
		client->StartAutoFocus(afMode, info);
	} else if (tool == ui->btn_af_normal) {
		afMode.set_af_mode(m11::AutoFocusQ::Mode::AutoFocusQ_Mode_AF_NORMAL);
		client->StartAutoFocus(afMode, info);
	} else if (tool == ui->btn_af_speed) {
		afMode.set_af_mode(m11::AutoFocusQ::Mode::AutoFocusQ_Mode_AF_FAST);
		client->StartAutoFocus(afMode, info);
	}
}

void MainWindow::on_btn_released()
{
	m11::LensGeneralInfo info;
	int ok;
#if 0
	QToolButton *tool = (QToolButton *)sender();
	if (tool == ui->btn_focus_in){
		qDebug() << "release  focus stop";
		ok = client->StopFocus(info);
	} else if (tool == ui->btn_focus_out){
		qDebug() << "release  focus stop";
		ok = client->StopFocus(info);
	} else if (tool == ui->btn_zoom_in){
		qDebug() << "release  zoom stop";
		ok = client->StopZoom(info);
	}else if (tool == ui->btn_zoom_out){
		qDebug() << "release  zoom stop";
		ok = client->StopZoom(info);
	}
#endif
}

void MainWindow::on_slider_zoom_speed_valueChanged(int value)
{
	zoomSpeed = (value * 20) / 100;
}

void MainWindow::on_slieder_focus_speed_valueChanged(int value)
{
	focusSpeed = (value * 20) / 100;
}

void MainWindow::on_slider_iris_pos_valueChanged(int value)
{
	value = value * 5 + 300;
	m11::IRISQ iris;
	m11::LensGeneralInfo info;
	iris.set_iris_position(value);
	client->SetIRISPosition(iris, info);
}

void MainWindow::on_comboBox_lens_type_currentIndexChanged(int index)
{
	
}

void MainWindow::on_comboBox_encoder_type_activated(int index)
{
	m11::StreamQ stream;
	stream.set_thread_no(0);
	m11::VideoGeneralInfo info;
	if (index == 0) {
		// H264 format selected;
		client->SetH264Format(stream, info);
	} else if (index == 1) {
		// HEVC format selected;
		client->SetHEVCFormat(stream, info);
	} else if (index == 2) {
		// mjpeg format selected;
	}
}

void MainWindow::timeout()
{
	if (!client)
		return;
	m11::IrCutFilter resp;
	client->getIrCutState(resp);
	m11::IrisState state;
	client->getIrisPos(state);
	qDebug() << "timeout" << resp.ircut_opened() << state.is_opened() << state.pos();
	m11::StreamQ stream;
	stream.set_thread_no(0);
	m11::VideoGeneralInfo info;
	client->GetStreamFormatType(stream, info);
	qDebug() << "Video strema stype" << ((info.stream_info()== 1) ? "HEVC" : "H264");
}


void MainWindow::on_line_focus_pos_returnPressed()
{
	m11::PosQ pos;
	m11::LensGeneralInfo info;
	pos.set_pos(ui->line_focus_pos->text().toInt());
	client->MoveToFocusPos(pos, info);
}

void MainWindow::on_line_zoom_pos_returnPressed()
{
	m11::PosQ pos;
	m11::LensGeneralInfo info;
	pos.set_pos(ui->line_zoom_pos->text().toInt());
	client->MoveToZoomPos(pos, info);
}

void MainWindow::on_line_cam_ip_returnPressed()
{
	client = new GrpcClient(ui->line_cam_ip->text(), 40001);
}
