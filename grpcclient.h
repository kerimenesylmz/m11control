#ifndef GRPCCLIENT_H
#define GRPCCLIENT_H

#include <QString>

#include "proto/m11control.grpc.pb.h"

class GrpcClient {
public:
	GrpcClient(QString host, int port);
		
	int SetZoomSpeed(m11::ZoomQ, m11::LensGeneralInfo &info);
	int SetFocusSpeed(m11::FocusQ, m11::LensGeneralInfo &info);
	
	int StartForwardZoom(m11::ZoomQ zoom, m11::LensGeneralInfo &info);
	int StartBackwardZoom(m11::ZoomQ zoom, m11::LensGeneralInfo &info);
	int StopZoom(m11::LensGeneralInfo &info);
	
	int StartForwardFocus(m11::FocusQ focus, m11::LensGeneralInfo &info);
	int StartBackwardFocus(m11::FocusQ focus, m11::LensGeneralInfo &info);
	int StopFocus(m11::LensGeneralInfo &info);
	
	int OpenIRIS(m11::LensGeneralInfo &info);
	int CloseIRIS(m11::LensGeneralInfo &info);
	int SetIRISPosition(m11::IRISQ, m11::LensGeneralInfo &info);
	
	int SetH264Format(m11::StreamQ stream, m11::VideoGeneralInfo &info);
	int SetHEVCFormat(m11::StreamQ stream, m11::VideoGeneralInfo &info);

	int GetStreamFormatType(m11::StreamQ stream, m11::VideoGeneralInfo &info);

	int OpenIrCut(m11::LensGeneralInfo &info);
	int CloseIrCut(m11::LensGeneralInfo &info);

	int StartAutoFocus(m11::AutoFocusQ afMode, m11::LensGeneralInfo &info);

	int MoveToFocusPos(m11::PosQ pos, m11::LensGeneralInfo &info);
	int MoveToZoomPos(m11::PosQ pos, m11::LensGeneralInfo &info);
	int GetLensInfo(m11::LensInfo &info);


	int getIrCutState(m11::IrCutFilter &resp);
	int getIrisPos(m11::IrisState &state);
private:
	std::unique_ptr<m11::VideoControl::Stub> mystub;
	QString _host;
	int _port;
};

#endif  // GRPCCLIENT_H
