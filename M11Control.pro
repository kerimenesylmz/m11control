#-------------------------------------------------
#
# Project created by QtCreator 2019-04-04T16:20:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = M11Control
TEMPLATE = app

CONFIG += c++11
LIBS += -L/usr/local/lib/
LIBS += -lprotobuf -lgrpc++

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        grpcclient.cpp \
    proto/m11control.grpc.pb.cc \
    proto/m11control.pb.cc

HEADERS += \
        mainwindow.h \
        grpcclient.h \
    proto/m11control.pb.h \
    proto/m11control.grpc.pb.h

FORMS += \
        mainwindow.ui
